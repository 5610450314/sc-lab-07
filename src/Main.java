
public class Main {
	
	public static void main(String[]args){
		TheaterManager theater = new TheaterManager();
		
		theater.buyTicketFrom_price(10);  // Show All Seat No. that availability reserve and have price = 10
		
		theater.buyTicketFrom_num("O", 11);    //  O 11
		
		theater.buyTicketFrom_num("O", 12);    //  O 12
		
		theater.buyTicketFrom_num("O", 13);    //  O 13 
		
		theater.buyTicketFrom_num("A", 12);    //  A 12   
		
		theater.buyTicketFrom_num("B", 4);     //  B 4
		
		theater.buyTicketFrom_price(50);  // Show All Seat No. that availability reserve and have price = 10
		
		theater.buyTicketFrom_price(20);  // Show All Seat No. that availability reserve and have price = 10
		
		theater.buyTicketFrom_num("B", 4);  //Cann't reserve because this seat was reserved
		
		theater.buyTicketFrom_num("D", 4);  //   D 4 reserve one seat that have price = 20
		
	}

}
