import java.util.HashMap;


public class Seat
{
	static int[][] seating = DataSeatPrice.ticketPrices;
	static HashMap<String,Integer> map_row = new HashMap<String,Integer>();
	static String[] leters = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
	
	public static int TransRow(String rkey)
	{
		int row = 0;
		for(int a=0;a<seating.length;a++)
		{
			map_row.put(leters[a],a);  // a = index start 0  b = seat Row start A : data in map_row
		}
		for(int a=0;a<seating.length;a++)
		{
			if (map_row.containsKey(rkey)){
				row = map_row.get(rkey);
			}
		}
		
		return row;
	}
	
	public static int TransCol(int ckey)
	{
		int col=0;
		if(ckey>0 && ckey<20){
			col= ckey-1;
		}
		return col;
	}
	
	
	
	
	public static void displaySeating()
    {
		System.out.println("Available Seats:");
		System.out.println("     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20");
		System.out.println("__|____________________________________________________________");
		
    	for (int row = 0; row <seating.length; row++) 
        {
    		System.out.print(leters[row]+" | ");
            for (int col = 0; col<seating[row].length; col++)
            { 
            	if(seating[row][col]==0){
            		System.out.print(" "+seating[row][col] + " ");
				}else{
					System.out.print(seating[row][col] + " ");
				}
            	
            } 
            System.out.println(" "); 
        }         
    }
	
	public static boolean availability(int r, int s)   //checks if the seat is available or not 
    { 
        if (seating[r][s] != 0) 
        { 
            return true; 
        }
        else  
        { 
            return false; 
        } 
    }
	
	
	public static void availabilityPrice(int p)     //shows the available seating the the price user entered 
    { 
        String seatPrice = " ";
        for(int row = 0; row<seating.length; row++) 
        { 
            for(int seat = 0; seat<seating[row].length; seat++) 
            { 
                if(seating[row][seat] == p) 
                { 
                	String row_leter = leters[row];
            		int col_price = seat+1;
                    //seatPrice = seatPrice + "["+row_leter+","+col_price+"]";
            		seatPrice = seatPrice +" "+row_leter+col_price;
                } 
            } 
        } 

        if(seatPrice != " ") 
        { 
            System.out.println("\nSeats cost : "+p+"\n : "+seatPrice); 
            System.out.println(); 
        }
        else 
        { 
            System.out.println("Sorry there are no available seating in that price."); 
        } 
    } 
	
	
	public static int[][] changeAvailability(int r, int s) 
    { 
        seating[r][s] = 0; 
        return seating; 
    }
	
	
	public int getPrice(int r,int c){
		return seating[r][c];
	}	
	
	
}
