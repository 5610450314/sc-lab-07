
public class TheaterManager 
{
	Seat seat = new Seat();
	public TheaterManager(){
		
	}
	
	public void buyTicketFrom_num(String r,int c)
	{
		int row = seat.TransRow(r);
		int col = seat.TransCol(c);
		
		seat.displaySeating();
		
		if(seat.availability(row, col))
		{
			System.out.println("\nThis seat "+r+c+" is available ,and price : "+seat.getPrice(row, col));
			System.out.println("Reserve Success!");
			System.out.println();
			seat.changeAvailability(row, col);
		}
		else
		{
			System.out.println("\nSorry, this seat "+r+c+" is not available."); 
            System.out.println();
		}
	}
	
	public void buyTicketFrom_price(int price)
	{ 
		seat.displaySeating();
		seat.availabilityPrice(price);
	}
	
	

}
